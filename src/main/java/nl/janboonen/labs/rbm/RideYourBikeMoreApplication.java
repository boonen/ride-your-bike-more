package nl.janboonen.labs.rbm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RideYourBikeMoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(RideYourBikeMoreApplication.class, args);
	}

}

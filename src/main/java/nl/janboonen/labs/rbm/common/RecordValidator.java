package nl.janboonen.labs.rbm.common;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import lombok.extern.slf4j.Slf4j;

import java.util.Set;

@Slf4j
public final class RecordValidator {

    private final static Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    /**
     * Validate all fields of a subject for correctness.
     *
     * @param subject the object to validate
     * @param <T> the object can be of any type
     */
    public static <T> void validate(T subject) {
        Set<ConstraintViolation<T>> violations = validator.validate(subject);
        if(!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
    }

}

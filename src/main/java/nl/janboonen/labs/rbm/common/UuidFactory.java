package nl.janboonen.labs.rbm.common;

import java.util.UUID;
 sealed public interface UuidFactory permits UuidFactory.RandomUuidFactory, UuidFactory.FixedUuidFactory {

    UUID generate();

    final class RandomUuidFactory implements UuidFactory {
        @Override
        public UUID generate() {
            return UUID.randomUUID();
        }
    }

    final class FixedUuidFactory implements UuidFactory {
        public static final String FIXED_UUID = "00000000-0000-0000-0000-000000000000";

        @Override
        public UUID generate() {
            return UUID.fromString(FIXED_UUID);
        }
    }

}

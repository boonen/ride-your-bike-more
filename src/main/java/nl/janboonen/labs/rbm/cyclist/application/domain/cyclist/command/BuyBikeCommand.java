package nl.janboonen.labs.rbm.cyclist.application.domain.cyclist.command;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Past;
import lombok.NonNull;
import nl.janboonen.labs.rbm.common.RecordValidator;
import nl.janboonen.labs.rbm.cyclist.application.domain.BikeId;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.time.LocalDate;

public record BuyBikeCommand(@TargetAggregateIdentifier BikeId id, String frameNumber, String brand, String model, LocalDate purchaseDate) {

    public BuyBikeCommand(@NonNull BikeId id, @NotEmpty String frameNumber, @NotEmpty String brand, String model, @NotEmpty @Past LocalDate purchaseDate) {
        this.id = id;
        this.frameNumber = frameNumber;
        this.brand = brand;
        this.model = model;
        this.purchaseDate = purchaseDate;
        RecordValidator.validate(this);
    }

}

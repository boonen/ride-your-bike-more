package nl.janboonen.labs.rbm.cyclist.application.ports.in;

import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UseCaseConfiguration {

    @Bean
    BuyBikeUseCase buyBikeUseCase(CommandGateway commandGateway) {
        return new BuyBikeUseCase(commandGateway);
    }

}

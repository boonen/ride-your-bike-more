package nl.janboonen.labs.rbm.cyclist.application.ports.in;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.janboonen.labs.rbm.cyclist.application.domain.cyclist.command.BuyBikeCommand;
import org.axonframework.commandhandling.gateway.CommandGateway;

@Slf4j
@RequiredArgsConstructor
public class BuyBikeUseCase {

    private final CommandGateway commandGateway;

    public String buyBike(BuyBikeCommand buyBikeCommand) {
        commandGateway.sendAndWait(buyBikeCommand);
        return buyBikeCommand.id().toString();
    }

}

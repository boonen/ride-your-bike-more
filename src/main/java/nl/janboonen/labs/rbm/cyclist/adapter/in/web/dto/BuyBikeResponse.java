package nl.janboonen.labs.rbm.cyclist.adapter.in.web.dto;

public record BuyBikeResponse(String bikeId) {
}

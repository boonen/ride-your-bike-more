package nl.janboonen.labs.rbm.cyclist.application.domain;

import jakarta.validation.constraints.NotNull;
import nl.janboonen.labs.rbm.common.RecordValidator;

import java.util.UUID;

public record BikeId(@NotNull UUID id) {

    public BikeId(UUID id) {
        this.id = id;
        RecordValidator.validate(this);
    }
}

package nl.janboonen.labs.rbm.cyclist.adapter.in.web.mapper;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import nl.janboonen.labs.rbm.common.UuidFactory;
import nl.janboonen.labs.rbm.cyclist.adapter.in.web.dto.BuyBikeRequest;
import nl.janboonen.labs.rbm.cyclist.adapter.in.web.dto.BuyBikeResponse;
import nl.janboonen.labs.rbm.cyclist.application.domain.BikeId;
import nl.janboonen.labs.rbm.cyclist.application.domain.cyclist.command.BuyBikeCommand;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper
@Slf4j
@Setter
public abstract class BuyBikeWebAdapterMapper {

    protected UuidFactory uuidFactory;

    @Mapping(target = "id", source = ".", qualifiedByName = "generateId")
    public abstract BuyBikeCommand toBuyBikeCommand(BuyBikeRequest buyBikeRequest);

    @Named("generateId")
    BikeId generateId(BuyBikeRequest buyBikeRequest) {
        return new BikeId(uuidFactory.generate());
    }

    @Mapping(target = "bikeId", source = ".")
    public abstract BuyBikeResponse toBuyBikeResponse(String bikeId);
}

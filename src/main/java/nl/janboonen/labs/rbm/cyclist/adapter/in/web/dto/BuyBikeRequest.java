package nl.janboonen.labs.rbm.cyclist.adapter.in.web.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Past;

import java.time.LocalDate;

public record BuyBikeRequest(@NotEmpty String frameNumber, @NotEmpty String brand, String model,
                             @NotNull @Past LocalDate purchaseDate) {

}

package nl.janboonen.labs.rbm.cyclist.adapter.in.web;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.janboonen.labs.rbm.cyclist.adapter.in.web.dto.BuyBikeRequest;
import nl.janboonen.labs.rbm.cyclist.adapter.in.web.dto.BuyBikeResponse;
import nl.janboonen.labs.rbm.cyclist.adapter.in.web.mapper.BuyBikeWebAdapterMapper;
import nl.janboonen.labs.rbm.cyclist.application.ports.in.BuyBikeUseCase;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/cyclist/api/v1")
public class BuyBikeController {

    private final BuyBikeUseCase buyBikeUseCase;
    private final BuyBikeWebAdapterMapper buyBikeWebAdapterMapper;

    @PostMapping
    public BuyBikeResponse buyBike(@RequestBody @Valid BuyBikeRequest buyBikeRequest) {
        var bikeId = buyBikeUseCase.buyBike(buyBikeWebAdapterMapper.toBuyBikeCommand(buyBikeRequest));
        return buyBikeWebAdapterMapper.toBuyBikeResponse(bikeId);
    }

}

package nl.janboonen.labs.rbm.cyclist.application.domain.cyclist;

import nl.janboonen.labs.rbm.cyclist.application.domain.BikeId;
import nl.janboonen.labs.rbm.cyclist.application.domain.cyclist.command.BuyBikeCommand;
import nl.janboonen.labs.rbm.cyclist.application.domain.cyclist.event.BikeBoughtEvent;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.common.Assert;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;

import java.time.LocalDate;

@Aggregate
public class OwnedBike {

    @AggregateIdentifier
    private BikeId id;
    private String frameNumber;
    private String brand;
    private String model;
    private LocalDate purchaseDate;
    private double mileage = 0.0;

    public OwnedBike() {
        // Axon requires an empty constructor
    }

    @CommandHandler
    public OwnedBike(BuyBikeCommand buyBikeCommand) {
        Assert.nonNull(buyBikeCommand.id(), () -> "ID should not be null");
        AggregateLifecycle.apply(new BikeBoughtEvent(buyBikeCommand.id(), buyBikeCommand.frameNumber(), buyBikeCommand.brand(), buyBikeCommand.model(), buyBikeCommand.purchaseDate()));
    }
    
    @EventSourcingHandler
    void on(BikeBoughtEvent event) {
        this.id = event.id();
        this.frameNumber = event.frameNumber();
        this.brand = event.manufacturer();
        this.model = event.model();
        this.purchaseDate = event.purchaseDate();
    }

}

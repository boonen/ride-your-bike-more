package nl.janboonen.labs.rbm.cyclist.infrastructure;

import jakarta.annotation.PostConstruct;
import org.axonframework.eventsourcing.eventstore.EventStorageEngine;
import org.axonframework.eventsourcing.eventstore.inmemory.InMemoryEventStorageEngine;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AxonConfiguration {

    @Bean
    public EventStorageEngine storageEngine() {
        return new InMemoryEventStorageEngine();
    }

    @PostConstruct
    public void setProperty() {
        System.setProperty("disable-axoniq-console-message", "true");
    }

}

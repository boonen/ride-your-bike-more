package nl.janboonen.labs.rbm.cyclist.adapter.in.web;

import nl.janboonen.labs.rbm.common.UuidFactory;
import nl.janboonen.labs.rbm.cyclist.adapter.in.web.mapper.BuyBikeWebAdapterMapper;
import org.mapstruct.factory.Mappers;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WebAdapterConfiguration {

    @Bean
    public BuyBikeWebAdapterMapper buyBikeWebAdapterMapper() {
        BuyBikeWebAdapterMapper mapper = Mappers.getMapper(BuyBikeWebAdapterMapper.class);
        mapper.setUuidFactory(new UuidFactory.RandomUuidFactory());
        return mapper;
    }

}

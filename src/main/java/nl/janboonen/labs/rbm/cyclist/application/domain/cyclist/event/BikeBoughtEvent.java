package nl.janboonen.labs.rbm.cyclist.application.domain.cyclist.event;

import nl.janboonen.labs.rbm.cyclist.application.domain.BikeId;

import java.time.LocalDate;

public record BikeBoughtEvent(BikeId id, String frameNumber, String manufacturer, String model, LocalDate purchaseDate) {
}

package nl.janboonen.labs.rbm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.TestConfiguration;

@TestConfiguration(proxyBeanMethods = false)
public class TestRideYourBikeMoreApplication {

	public static void main(String[] args) {
		SpringApplication.from(RideYourBikeMoreApplication::main).with(TestRideYourBikeMoreApplication.class).run(args);
	}

}

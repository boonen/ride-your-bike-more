# Ride your Bike More

Riding a bicycle is fun, healthy and environment-friendly. So, shouldn't we all ride our bikes more? The Ride your Bike More (RBM) application helps us to use
bikes more often as a means of transportation. The application's name is abbreviated as RBM.

## About the project

### Showcasing Domain-Driven Design

#### Domain Storytelling

The following diagram was created using [Egon](https://egon.io) and simplifies the problem that the RBM application will solve:

![Domain Storytelling diagram](docs/rbm_domain_storytelling.svg)

The focus of the application will be the end user or "Cyclist". A Cyclist will be able to search a catalogue that is maintained by Bike Manufacturers. Using
the catalogue a Cyclist is able to buy a bike from a preferred Bike Store. The Cyclist is then responsible for registering the bike's metrics (either manually
or automatically using a connected sensor; e.g. using Bluetooth). The data is collected by the RBM service and once a Service Milestone is reached the Bike
Shop is notified, so they can schedule maintenance and invite the Cyclist to bring in the Bike.

#### Event Storming

Based on the Domain Storytelling Diagram we can do simple event storming to identify the events that are important to the RBM application. In the folowing
paragraphs we refer to the Storytelling Diagram using the scenario numbers.

##### Scenario 5: buying a bike

```plantuml
@startuml
' constants
!global $ICON_FORMAT="png"
!global $TEXT_WIDTH_MAX=200
!global $MSG_WIDTH_MAX=150
!global $FONT_SIZE_XS=10
!global $FONT_SIZE_SM=12
!global $FONT_SIZE_MD=16
!global $FONT_SIZE_LG=20
!global $FONT_COLOR="#212121"
!global $FONT_COLOR_LIGHT="#757575"

' Styles
hide stereotype
skinparam wrapWidth $TEXT_WIDTH_MAX
skinparam maxMessageSize $MSG_WIDTH_MAX
skinparam DefaultFontSize $FONT_SIZE_SM
skinparam DefaultFontColor $FONT_COLOR

skinparam file<<Command>> {
    StereotypeFontSize $FONT_SIZE_SM
    shadowing false
    FontColor $FONT_COLOR
    BorderColor $FONT_COLOR
    BackgroundColor #aec6cf
}
skinparam file<<Event>> {
    StereotypeFontSize $FONT_SIZE_SM
    shadowing false
    FontColor $FONT_COLOR
    BorderColor $FONT_COLOR
    BackgroundColor #ffb853
}
skinparam file<<DomainEvent>> {
    StereotypeFontSize $FONT_SIZE_SM
    shadowing false
    FontColor $FONT_COLOR
    BorderColor $FONT_COLOR
    BackgroundColor #ffcb81
}
skinparam file<<Query>> {
    StereotypeFontSize $FONT_SIZE_SM
    shadowing false
    FontColor $FONT_COLOR
    BorderColor $FONT_COLOR
    BackgroundColor #62d862
}
skinparam file<<ReadModel>> {
    StereotypeFontSize $FONT_SIZE_SM
    shadowing false
    FontColor $FONT_COLOR
    BorderColor $FONT_COLOR
    BackgroundColor #77dd77
}
skinparam file<<UserInterface>> {
    StereotypeFontSize $FONT_SIZE_SM
    shadowing false
    FontColor $FONT_COLOR
    BorderColor $FONT_COLOR
    BackgroundColor #a2e8a2
}
skinparam file<<Aggregate>> {
    StereotypeFontSize $FONT_SIZE_SM
    shadowing false
    FontColor $FONT_COLOR
    BorderColor $FONT_COLOR
    BackgroundColor #fdfd9d
}
skinparam file<<Service>> {
    StereotypeFontSize $FONT_SIZE_SM
    shadowing false
    FontColor $FONT_COLOR
    BorderColor $FONT_COLOR
    BackgroundColor #fcfc78
}
skinparam file<<Policy>> {
    StereotypeFontSize $FONT_SIZE_SM
    shadowing false
    FontColor $FONT_COLOR
    BorderColor $FONT_COLOR
    BackgroundColor #b6a2db
}
skinparam file<<Saga>> {
    StereotypeFontSize $FONT_SIZE_SM
    shadowing false
    FontColor $FONT_COLOR
    BorderColor $FONT_COLOR
    BackgroundColor #c9bbe5
}

person Cyclist <<Person>>
file BuyBikeCommand <<Command>>
file BikeBoughtEvent <<Event>>
file BikeShopService <<Service>>

Cyclist -> BuyBikeCommand
BuyBikeCommand -> BikeShopService
BikeShopService -> BikeBoughtEvent

@enduml
```

### Handy examples of unit and integration tests

The project's main aim is to provide a simple, appealing application that resembles a real life application. From this
starting point it shows you how you can best test the different components of a software system.

## Installation

Before you can start testing the Ride your Bike More application you will need a working [Maven3 installation](https://maven.apache.org) and a working
[Docker daemon](https://docker.com). Now you can build the application using Maven:

```shell
mvn package
```

If the build is successful you can start the application using Docker:

```shell
docker compose up -d
```

## Usage

We advise [Bruno](https://usebruno.com) to interact with RBM's REST interfaces. Once the application is running you can use the supplied Bruno scripts to test
the different use cases.

## Support

Looking for support? Leave an issue in GitLab and interact with the developers.

## Roadmap

The following ideas and features are expected soon:

- [ ] Showcase of testing Anti-Corruption Layer using Testcontainers
- [ ] Showcase of testing architecture with ArchUnit and Spring Modulith
- [ ] Implementation using the Axon Framework 

## License

This project is licensed under the [MIT license](LICENSE.md).
